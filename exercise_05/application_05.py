from flask import Flask, request, jsonify
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Order


engine = create_engine('sqlite:///orders.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

app = Flask(__name__)


@app.route("/")
@app.route("/orders", methods = ['GET', 'POST'])
def ordersFunction():
    if request.method == 'GET':
        #Call the method to Get all of the orders
        return getAllorders()
    elif request.method == 'POST':
        #Call the method to make a new order
        print("Making a New order")

    name = request.args.get('name', '')
    description = request.args.get('description', '')
    print(name)
    print(description)
    return makeANeworder(name, description)



#Make another app.route() decorator here that takes in an integer id in the URI
@app.route("/orders/<int:id>", methods = ['GET', 'PUT', 'DELETE'])
#Call the method to view a specific order
def ordersFunctionId(id):
    if request.method == 'GET':
        return getorder(id)

    #Call the method to edit a specific order
    elif request.method == 'PUT':
        name = request.args.get('name', '')
        description = request.args.get('description', '')
        return updateorder(id,name, description)

    #Call the method to remove a order
    elif request.method == 'DELETE':
        return deleteorder(id)

def getAllorders():
    orders = session.query(Order).all()
    return jsonify(orders=[i.serialize for i in orders])

def getorder(id):
    # Implement find and return an order by ID
    return True

def makeANeworder(name,description):
    order = order(name = name, description = description)
    session.add(order)
    session.commit()
    return jsonify(order=order.serialize)

def updateorder(id,name, description):
    order = session.query(Order).filter_by(id = id).one()
    if not name:
        order.name = name
    if not description:
        order.description = description
    session.add(order)
    session.commit()
    return "Updated a order with id %s" % id

def deleteorder(id):
    # Implement deleting an order
    return True


if __name__ == '__main__':
    app.debug = False
    app.run(host='0.0.0.0', port=5000)
