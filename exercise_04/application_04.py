from flask import Flask, request

app = Flask(__name__)

@app.route("/orders", methods = ['GET', 'POST'])
def ordersFunction():
  if request.method == 'GET':
  	#Call the method to Get all of the orders
  	return getAllOrders()

  elif request.method == 'POST':
  	#Call the method to make a new order
  	return makeANewOrder()

# Implement missing methods and lines of code
@app.route("/orders/<int:id>", methods = ['GET', 'PUT', 'DELETE'])
def ordersFunctionId(id):
  if request.method == 'GET':
  	# Add missing method and replace the line below
    return True
  if request.method == 'PUT':
    # Add missing method and replace the line below
    return True
  elif request.method == 'DELETE':
  	# Add missing method and replace the line below
    return True


def getAllOrders():
  return "Getting All the orders!"

def makeANewOrder():
  return "Creating A New Order!"


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
