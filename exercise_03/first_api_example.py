from flask import Flask
app = Flask(__name__)

# Add the rest of CRUD method to this file

# What's missing?
# DELETE
# UPDATE

# CREATE
@app.route('/orders', methods=['POST'])
def create():
    return "I created an order"

# INDEX
@app.route('/orders', methods=['GET'])
def index():
    return "Yes, all the orders!"

# SHOW
@app.route('/orders/<int:id>', methods=['GET'])
def show(id):
    return "This method will act on the order with id %s" % id

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
